// The Bubble sort Implementation using Javascript
// Creating the bblSort function
function bubbleSort(arr) {
  for (let x = 0; x < arr.length; x++) {
    // Last i elements are already in place
    for (let y = 0; y < arr.length - x - 1; y++) {
      // Checking if the item at present iteration
      // is greater than the next iteration
      if (arr[y] > arr[y + 1]) {
        // If the condition is true then swap them
        let temp = arr[y];
        arr[y] = arr[y + 1];
        arr[y + 1] = temp;
      }
    }
  }
  // Print the sorted array
  console.log(arr);
}

// This is our unsorted array
let arr = ["g", "u", "s", "t", "i", "a", "r", "s", "y", "a", "d"];

// Now pass this array to the bubbleSort() function
bubbleSort(arr);
