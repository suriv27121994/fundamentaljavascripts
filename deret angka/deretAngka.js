function deretAngka(n) {
  const kata1 = "Coba";
  const kata2 = "Tes";
  const kata3 = "Anda";

  let hasilDeretAngka = "";

  for (let i = 1; i <= n; i++) {
    if (i % 5 === 0 && i % 3 === 0) hasilDeretAngka += `${kata1}${kata2}${kata3} `;
    if (i % 3 === 0) hasilDeretAngka += `${kata1} `;
    if (i % 5 === 0) hasilDeretAngka += `${kata2} `;
    if (i % 8 === 0) hasilDeretAngka += `${kata3} `;
  }

  return hasilDeretAngka;
}

console.log(deretAngka(5));
console.log(deretAngka(10));
console.log(deretAngka(15));
