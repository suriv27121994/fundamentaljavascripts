test("Test toBe", () => {
  const name = "Gusti";
  const hello = `Hello ${name}`;

  expect(hello).toBe("Hello Gusti");
});

test("Test toEquals", () => {
  let person = {id: "Gusti"};
  Object.assign(person, {name: "Arsyad"});

  expect(person).toEqual({id: "Gusti", name: "Arsyad"});
});
