import { sum } from "../src/sum";

test("Test Function 1", () => {
  const result = sum(5, 5);

  expect(result).toBe(10);
});

test("Test Function 2", () => {
  const result = sum(50, 5);

  expect(result).toBe(55);
});

test("Test Function 3", () => {
  const result = sum(1, 5);

  expect(result).toBe(6);
});