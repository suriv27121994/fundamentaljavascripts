/*variable of let */
let a = 10;
console.log(a);

//you can not do this
//let a =20; //because a has been declared

//except do this  akan berubah menjadi 20
a = 20;
console.log(a);

/*Variable of var */
var b = 5;
console.log(b);

//you can do this
var b = 10;
console.log(b);

var b = 20;
console.log(b);