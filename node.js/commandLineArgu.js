// The process.argv is the array that contains command line arguments and  print all arguments using forEach
process.argv.forEach((val, idx) => {
  console.log(`${idx}: ${val}`);
});
