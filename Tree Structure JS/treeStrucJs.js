const output = (string) => (document.body.innerHTML += `${string}<br/>`);

//elements of the tree
class Node {
  constructor(data) {
    this.data = data;
    this.parent = null;
    this.children = [];
  }
}

//the tree datastructure
class Tree {
  constructor(data) {
    let node = new Node(data);
    this._root = node;
  }

  //returns the node that has the given data, or null
  //use a depth-first search
  find(data, node = this._root) {
    //if the current node matches the data, return it
    if (node.data == data) return node;

    //recurse on each child node
    for (let child of node.children) {
      //if the data is found in any child node it will be returned here
      if (this.find(data, child)) return child;
    }

    //otherwise, the data was not found
    return null;
  }

  //create a new node with the given data and add it to the specified parent node
  add(data, DataOfParent) {
    let node = new Node(data);
    let parent = this.find(DataOfParent);

    //if the parent exists, add this node
    if (parent) {
      parent.children.push(node);
      node.parent = parent;

      //return this node
      return node;
    }
    //otherwise throw an error
    else {
      throw new Error(
        `Cannot add node: parent with data ${DataOfParent} not found.`
      );
    }
  }

  //removes the node with the specified data from the tree
  remove(data) {
    //find the node
    let node = this.find(data);

    //if the node exists, remove it from its parent
    if (node) {
      //find the index of this node in its parent
      let parent = node.parent;
      let indexOfNode = parent.children.indexOf(node);
      //and delete it from the parent
      parent.children.splice(indexOfNode, 1);
    }
    //otherwise throw an error
    else {
      throw new Error(`Cannot remove node: node with data ${data} not found.`);
    }
  }

  //depth-first tree traversal
  //starts at the root
  forEach(callback, node = this._root) {
    //recurse on each child node
    for (let child of node.children) {
      //if the data is found in any child node it will be returned here
      this.forEach(callback, child);
    }

    //otherwise, the data was not found
    callback(node);
  }

  //breadth-first tree traversal
  forEachBreadthFirst(callback) {
    //start with the root node
    let queue = [];
    queue.push(this._root);

    //while the queue is not empty
    while (queue.length > 0) {
      //take the next node from the queue
      let node = queue.shift();

      //visit it
      callback(node);

      //and enqueue its children
      for (let child of node.children) {
        queue.push(child);
      }
    }
  }
}

let tesTree = new Tree("CEO");
tesTree.add("VSP Finance", "CEO");
tesTree.add("VSP Sales", "CEO");
tesTree.add("Sales of person", "VSP Sales");
tesTree.add("The Accountant", "VSP Finance");
tesTree.add("The Bookkeeper", "VSP Finance");

console.log("Depth-First\n");
tesTree.forEach((node) => console.log(" - " + node.data));
console.log("\nBreadth-First\n");
tesTree.forEachBreadthFirst((node) => console.log(" - " + node.data));
