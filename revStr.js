function revStr(str) {
  // Step 1. Use the split() method to return a new array
  let splitString = str.split(""); // let splitString = "Gusti Arsayd".split("");
  // ["G", "u", "s", "t", "i", " ", "A", "r", "s", "y", "a", "d"]
  console.log(splitString);

  // Step 2. Use the reverse() method to reverse the new created array
  let revArr = splitString.reverse(); // let reverseArray = ['d', 'a', 'y', 's', 'r', 'A', ' ', 'i', 't', 's', 'u', 'G']
  console.log(revArr);

  // Step 3. Use the join() method to join all elements of the array into a string
  let joinArray = revArr.join(""); // let joinArray = ["d", "a", "y", "s","r", "A", " ", "i", "t", "s", "u", "G"].join("");
  // "daysrA itsuG"
  console.log(joinArray);

  //Step 4. Return the reversed string
  return joinArray; // "daysrA itsuG"
}

console.log(revStr("Gusti Arsyad"));
