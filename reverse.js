function ReverseString(str) {
	
	// Take empty array revArray
	const revArr = [];
	const length = str.length - 1;
	
	// Looping from the end
	for(let i = length; i >= 0; i--) {
		revArr.push(str[i]);
	}
	
	// Joining the array elements
	return revArr.join('');
}

console.log(ReverseString("Gusti Arsyad"));

